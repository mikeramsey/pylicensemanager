# Pylicensemanager

PyLicenseManager is a Python client library for the License Manager for Woocommerce Rest API.

## Installation

Use the package manager [pip](https://pypi.org/) to install pylicensemanager.

`
pip install pylicensemanager
`

## Usage

```
import pylicensemanager
```

Review documentation for more information: https://www.licensemanager.at/docs/rest-api/

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)